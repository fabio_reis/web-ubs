var map, currentMark;
var wrapperInfo = document.querySelector(".wrapper-info");

//função de callback para iniciar o mapa
function initMap() {
    var options = {        
        center: {lat: -23.567900, lng: -46.649200},
        zoom: 12,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#444444"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#46bcec"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ]       
    };

    //instancia o obj map da API de mapas do google
    map = new google.maps.Map(document.getElementById('map'), options);    
   
    //verifica se o navegador tem suporte a geolocalização
    if(navigator.geolocation){

        //pega a posição atual do usuario
        navigator.geolocation.getCurrentPosition(position => {

            let currentPosition = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            //marca no map o local atual
            addMarker(currentPosition,'Aqui')    
            
            //busca as UBSs no servidor
            getUBSs(currentPosition.lat,currentPosition.lng);

        },
        (error) => { alert("Serviço de localização falhou") } //callback de erro
    );
    //caso o navegador não tenha suporte a localização, seta uma mensagem personalizada
    }else
        alert("O navegador não possuo suporte para Geolocalização");   
}

//busca as UBSs no servidor
function getUBSs(lat,lng){

    if(self.fetch){

        //requisição GET para o servidor local
        fetch('http://localhost:8080/api/ubs?geocodeLat=' + lat + '&geocodeLong=' + lng,{
            method: 'GET',
            headers: {'Content-Type' : 'application/json;charset=UTF-8'}      
        })        
        .then(response => response.json())
        .then(UBSs => {

            //marca as UBSs no mapa e insere na caixa de informação.
            UBSs.forEach(element => {
                
                let currentLocation = {
                    lat: element.geocodeLat,
                    lng: element.geocodeLong
                }   
                
                addMarker(currentLocation,'UBS','img/icon2.png');

                createDOMElement(element);

            });      

        }).catch(e => alert("Erro ao buscar as UBSs: erro interno de servidor"));
    }else
        alert("Esse navegador não pode acessar o servidor, use o Google chrome");

}

//marca a posição no mapa
function addMarker(location,title,icon) {
    var marker = new google.maps.Marker({
        position: location,
        title: title, 
        icon: icon,       
        map:map
    });
  }

  //cria um elemento no DOM
  function createDOMElement(ubs){

    let divInfo = document.createElement('div');
    divInfo.classList += "info";

    divInfo.innerHTML =   
            `<p>Nome: <span>${ubs.name}<span></p>` +
            `<p>Distância: <span>${ubs.distance.toFixed(2)} Km<span></p>`;

    wrapperInfo.appendChild(divInfo);

  }


    